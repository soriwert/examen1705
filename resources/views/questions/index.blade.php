@extends('layouts.app')
@section('content')
@auth
<h1>Lista de questions</h1>
<table class="table table-bordered">
    <tr>
        <th>Lista de Exámenes</th>
    </tr>

    @foreach ($questions as $question)
    <tr>
        <td>{{ $question->enunciado }}</td>

        @if($question->answer  == 'a')
        <td class="bg-success">{{ $question->a }}</td>
        @else
        <td>{{ $question->a }}</td>
        @endif

        @if($question->answer == 'b')
        <td class="bg-success">{{ $question->b }}</td>
        @else
        <td>{{ $question->b }}</td>
        @endif

        @if($question->answer == 'c')
        <td class="bg-success">{{ $question->c }}</td>
        @else
        <td>{{ $question->c }}</td>
        @endif

        @if($question->answer  == 'd')
        <td class="bg-success">{{ $question->d }}</td>
        @else
        <td>{{ $question->d }}</td>
        @endif

        <td>{{ $question->answer }}</td>

    </tr>
    @endforeach
</table>
<hr>
@endauth
@endsection
