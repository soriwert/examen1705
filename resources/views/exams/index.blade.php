@extends('layouts.app')
@section('content')
@auth
<h1>Lista de grupos</h1>
<table class="table table-bordered">
    <tr>
        <th>Lista de Exámenes</th>
    </tr>

    @foreach ($exams as $exam)
    <tr>
        <td>{{ $exam->user->name }}</td>
        <td>{{ $exam->title }}</td>
        <td>{{ $exam->date }}</td>
        <td>{{ $exam->module->name }}</td>

    </tr>
    @endforeach
</table>
<hr>
@endauth
@endsection
