<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = ['id', 'date', 'title', 'materia_id', 'module', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question');
    }
}
