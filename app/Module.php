<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{

    protected $fillable = ['id', 'code', 'name'];

    public function Exams()
    {
        return $this->hasMany('App\Exam');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
