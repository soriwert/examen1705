<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['id', 'text', 'a', 'b', 'c', 'd', 'answer', 'module_id', 'module'];

    public function exam()
    {
        return $this->belongsToMany('App\Exam');
    }

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

}
