<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{

    public function index()
    {
        $questions = Question::all();
        return view('questions.index', ['questions' => $questions]);
    }

    public function create()
    {
        return view('questions.create');
    }

    public function store(Request $request)
    {
         $this->validate($request, [
            'text' => 'required|max:10|',
            'a' => 'required|max:10|',
            'b' => 'required|max:10|',
            'c' => 'required|max:10|',
            'd' => 'required|max:10|',
            'answer' => 'required|max:1',
        ]);
        $question = new Question($request->all());
        $question->save();
        $request->session()->put('lastQuestion', $question->id);
        return redirect('/questions');
    }

}
